cbuffer ConstantColorBuff
{
	/*
	float red;
	float green;
	float blue;
	*/
	matrix matrusken;
};

struct vs_in {
	float3 position_local : POS; 
	float3 incolor: COL;
};

struct vs_out {
	float4 position_clip : SV_POSITION;
	float3 outcolor : COLOR;
};

//vs_out vs_main(vs_in input) {
vs_out vs_main(float3 pos: POS, float3 col :COL) {
	vs_out output = (vs_out)0;
	//output.position_clip = float4(input.position_local, 1.0);
//	output.outcolor = input.incolor;
	//float4 outputCol = { input.incolor.r, input.incolor.g, input.incolor.b, 1.0f };
	float4 outputCol = { col.r, col.g, col.b, 1.0f };

	/*
	outputCol.r *= red;
	outputCol.g *= green;
	outputCol.b *= blue;
	*/
	float4 myPos = { pos, 1.0f };
	//myPos.x = pos.x * cos(green) - pos.y * sin(green);
	//myPos.y = pos.x * sin(green) + pos.y * cos(green);
	//myPos.x *= green;
	output.outcolor = outputCol;
		
	//output.position_clip = myPos;
	output.position_clip = mul(  matrusken, myPos);
	//output.position_clip = float4((pos.x + red), pos.y, pos.z, 1.0);
	return output;
};

float4 ps_main(vs_out input) : SV_TARGET {
	return float4(input.outcolor, 1.0);
};