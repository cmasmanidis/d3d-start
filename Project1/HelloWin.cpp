
#ifndef UNICODE
#define UNICODE
#endif
//#define _CRT_SECURE_NO_WARNINGS
#define WM_LBUTTONDOWN 0x0201

#include <windows.h>
#include <WinDef.h>
#include <windowsx.h>
#include <stdio.h>
#include <ctime>
#include <directxmath.h>

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <d3d11.h>       // D3D interface
#include <dxgi.h>        // DirectX driver interface
#include <d3dcompiler.h> // shader compiler
#include <cassert>
#include <corecrt_math.h>

#pragma comment( lib, "user32" )          // link against the win32 library
#pragma comment( lib, "d3d11.lib" )       // direct3D library
#pragma comment( lib, "dxgi.lib" )        // directx graphics interface
#pragma comment( lib, "d3dcompiler.lib" ) // shader compiler

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);


double win32GetCurrentTimeInSeconds(LARGE_INTEGER perfCounterFrequency)
{
	LARGE_INTEGER perfCount;
	QueryPerformanceCounter(&perfCount);

	double result = (double)perfCount.QuadPart / (double)perfCounterFrequency.QuadPart;

	return result;
}


int WINAPI wWinMain(
	_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ PWSTR pCmdLine,
	_In_ int nCmdShow)
{

	//D3D stuff

	ID3D11Device* device_ptr = NULL;
	ID3D11DeviceContext* device_context_ptr = NULL;
	IDXGISwapChain* swap_chain_ptr = NULL;
	ID3D11RenderTargetView* render_target_view_ptr = NULL;


	const wchar_t CLASS_NAME[] = L"Sample Window Class";
	WNDCLASS wc = {};

	wc.lpfnWndProc = WindowProc;
	wc.hInstance = hInstance;
	wc.lpszClassName = CLASS_NAME;

	RegisterClass(&wc);

	HWND hwnd = CreateWindowEx(
		0,
		CLASS_NAME,
		L"Learn to Program Windows",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	if (hwnd == NULL)
	{
		return 0;
	}

	DXGI_SWAP_CHAIN_DESC swap_chain_descr = { 0 };
	swap_chain_descr.BufferDesc.RefreshRate.Numerator = 0;
	swap_chain_descr.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	swap_chain_descr.SampleDesc.Count = 1;
	swap_chain_descr.SampleDesc.Quality = 0;
	swap_chain_descr.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swap_chain_descr.BufferCount = 1;
	swap_chain_descr.OutputWindow = hwnd;
	swap_chain_descr.Windowed = true;
	
	
	D3D_FEATURE_LEVEL feature_level ;

	UINT flags = D3D11_CREATE_DEVICE_SINGLETHREADED;
#if defined(DEBUG) || defined(_DEBUG)
	flags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	HRESULT hr = D3D11CreateDeviceAndSwapChain(
		NULL,
		D3D_DRIVER_TYPE_HARDWARE,
		NULL,
		flags,
		NULL,
		0,
		D3D11_SDK_VERSION,
		&swap_chain_descr,
		&swap_chain_ptr,
		&device_ptr,
		&feature_level,
		&device_context_ptr);
	assert(S_OK == hr && swap_chain_ptr && device_ptr && device_context_ptr);

	ID3D11Texture2D* framebuffer;
	hr = swap_chain_ptr->GetBuffer(
		0,
		__uuidof(ID3D11Texture2D),
		(void**)&framebuffer);
	assert(SUCCEEDED(hr));	

	hr = device_ptr->CreateRenderTargetView(
		framebuffer, 0, &render_target_view_ptr);
	assert(SUCCEEDED(hr));
	framebuffer->Release();


	 flags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG) || defined (_DEBUG)
	flags |= D3DCOMPILE_DEBUG;
#endif
	ID3DBlob* vs_blob_ptr = NULL, * ps_blob_ptr = NULL, * error_blob = NULL;

	 hr = D3DCompileFromFile(
		L"Shaders.hlsl",
		nullptr,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"vs_main",
		"vs_5_0",
		flags,
		0,
		&vs_blob_ptr,
		&error_blob);
	if (FAILED(hr)) {
		if (error_blob) {
			OutputDebugStringA((char*)error_blob->GetBufferPointer());
		}
		if (vs_blob_ptr) {
			vs_blob_ptr->Release();
		}
		assert(false);
	}

	hr = D3DCompileFromFile(
		L"shaders.hlsl",
		nullptr,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"ps_main",
		"ps_5_0",
		flags,
		0,
		&ps_blob_ptr,
		&error_blob);
	if (FAILED(hr)) {
		if (error_blob) {
			OutputDebugStringA((char*)error_blob->GetBufferPointer());
			error_blob->Release();
		}
		if (ps_blob_ptr) { ps_blob_ptr->Release(); }
		assert(false);
	}

	ID3D11VertexShader* vertex_shader_ptr = NULL;
	ID3D11PixelShader* pixel_shader_ptr = NULL;

	hr = device_ptr->CreateVertexShader(
		vs_blob_ptr->GetBufferPointer(),
		vs_blob_ptr->GetBufferSize(),
		NULL,
		&vertex_shader_ptr);
	assert(SUCCEEDED(hr));

	hr = device_ptr->CreatePixelShader(
		ps_blob_ptr->GetBufferPointer(),
		ps_blob_ptr->GetBufferSize(),
		NULL,
		&pixel_shader_ptr);
	assert(SUCCEEDED(hr));

	// REad this again jo

	ID3D11InputLayout* input_layout_ptr = NULL;
	D3D11_INPUT_ELEMENT_DESC inputElementDesc[] = {
	  { "POS", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	  { "COL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	  /*
	  { "NOR", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	  { "TEX", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	  */
	};
	hr = device_ptr->CreateInputLayout(
		inputElementDesc,
		ARRAYSIZE(inputElementDesc),
		vs_blob_ptr->GetBufferPointer(),
		vs_blob_ptr->GetBufferSize(),
		&input_layout_ptr);
	assert(SUCCEEDED(hr));

	float vertex_data_array[] = {
   0.0f,  0.5f,  0.0f, 1.0f, 1.0f, 0.0f,// point at top
   0.2f, -0.5f,  -0.25f, 1.0f, 0.0f, 1.0f, // point at bottom-right
  -0.2f, -0.5f,  -0.25f, 0.0f, 1.0f, 0.0f, // point at bottom-left

  0.0f,  0.5f,  0.0f, 1.0f, 1.0f, 0.0f,// point at top
  -0.2f, -0.5f,  -0.25f, 0.0f, 1.0f, 0.0f, // point at bottom-left
   -0.0f, -0.5f,  0.25f, 1.0f, 1.0f, 1.0f, // point at bottom-right

  0.0f,  0.5f,  0.0f, 1.0f, 1.0f, 0.0f,// point at top
  -0.0f, -0.5f,  0.25f, 1.0f, 1.0f, 1.0f, // point at bottom-right
   0.2f, -0.5f,  -0.25f, 1.0f, 0.0f, 1.0f, // point at bottom-right
  
	};
	UINT vertex_stride = 6 * sizeof(float);
	UINT vertex_offset = 0;
	UINT vertex_count = 9;

	ID3D11Buffer* vertex_buffer_ptr = NULL;
	{ /*** load mesh data into vertex buffer **/
		D3D11_BUFFER_DESC vertex_buff_descr = {};
		vertex_buff_descr.ByteWidth = sizeof(vertex_data_array);
		vertex_buff_descr.Usage = D3D11_USAGE_DEFAULT;
		vertex_buff_descr.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		D3D11_SUBRESOURCE_DATA sr_data = { 0 };
		sr_data.pSysMem = vertex_data_array;
		HRESULT hr = device_ptr->CreateBuffer(
			&vertex_buff_descr,
			&sr_data,
			&vertex_buffer_ptr);
		assert(SUCCEEDED(hr));
	}
	
	ID3D11Buffer* constantColorBuffer = NULL;
	{
		CD3D11_BUFFER_DESC cbd = { };
		cbd.ByteWidth = 64;
		cbd.Usage = D3D11_USAGE_DEFAULT;
		cbd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

		HRESULT hr = device_ptr->CreateBuffer(
			&cbd, nullptr, &constantColorBuffer);
		assert(SUCCEEDED(hr));
		device_context_ptr->VSSetConstantBuffers(0, 1, &constantColorBuffer);

	}

	ShowWindow(hwnd, nCmdShow);

	MSG msg = {};
	bool should_close = false;

	struct ColourLevels
	{
	//	float r, g, b;
		
		DirectX::XMMATRIX matrisken;
		
	};

	//ColourLevels colourLevels = { 1.0f, 1.0f, 1.0f };
	ColourLevels colourLevels = {};

	colourLevels.matrisken = DirectX::XMMatrixIdentity();

	while (!should_close)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		if (msg.message == WM_QUIT) { break; }

		{
			LARGE_INTEGER perfCounterFrequency;
			QueryPerformanceFrequency(&perfCounterFrequency);
			
			double currentTimeInSeconds = win32GetCurrentTimeInSeconds(perfCounterFrequency);
			

			/* clear the back buffer to cornflower blue for the new frame */
			float background_colour[4] = {
			  0x64 / 255.0f, 0x95 / 255.0f, 0xED / 255.0f, 1.0f };
			device_context_ptr->ClearRenderTargetView(
				render_target_view_ptr, background_colour);

			//std:time_t tiden = std::time(0);			
			//std::clock_t klok = std::clock();
			//colourLevels.r = sin(klok)/ 10; //0.0025f;
			//colourLevels.g = cos(klok ); //0.0025f;
			//colourLevels.b = sin(klok) ; //0.0025f;
			//colourLevels.g = sin(currentTimeInSeconds);
			//colourLevels.matrisken = DirectX::XMMatrixRotationY(0.087f);
			colourLevels.matrisken = DirectX::XMMatrixTranspose( DirectX::XMMatrixTranslation(0.0f, 0.0f, 3.0f) * DirectX::XMMatrixPerspectiveFovLH(1.0f, 3.0f/ 4.0f, 0.1f, 10.0f)* DirectX::XMMatrixRotationY(currentTimeInSeconds));
			

			//colourLevels.g -= 0.00125f;
			

			device_context_ptr->UpdateSubresource(constantColorBuffer, 0, 0, &colourLevels, 0, 0);

			RECT winRect;
			GetClientRect(hwnd, &winRect);
			D3D11_VIEWPORT viewport = {
			  0.0f,
			  0.0f,
			  (FLOAT)(winRect.right - winRect.left),
			  (FLOAT)(winRect.bottom - winRect.top),
			  0.0f,
			  1.0f };
			device_context_ptr->RSSetViewports(1, &viewport);

			device_context_ptr->OMSetRenderTargets(1, &render_target_view_ptr, NULL);

			device_context_ptr->IASetPrimitiveTopology(
				D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
			device_context_ptr->IASetInputLayout(input_layout_ptr);
			device_context_ptr->IASetVertexBuffers(
				0,
				1,
				&vertex_buffer_ptr,
				&vertex_stride,
				&vertex_offset);

			device_context_ptr->VSSetShader(vertex_shader_ptr, NULL, 0);
			device_context_ptr->PSSetShader(pixel_shader_ptr, NULL, 0);
		
			device_context_ptr->Draw(vertex_count, 0);
			swap_chain_ptr->Present(1, 0);
		}

	}
}

void OnSize(HWND hwnd, UINT flag, int width, int height)
{
	
}
LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_DESTROY:
	{
		PostQuitMessage(0);
		return 0;

	}
/*
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hwnd, &ps);
		
		FillRect(hdc, &ps.rcPaint, (HBRUSH)(COLOR_WINDOW + 10));

		EndPaint(hwnd, &ps);
		return 0;
	}
*/

	case WM_LBUTTONDOWN:
	{
		int xPos = GET_X_LPARAM(lParam);
		int yPos = GET_Y_LPARAM(lParam);
		char msgbuf[100];
		//sprintf(msgbuf, "X: %d Y: %d\n", xPos, yPos);
		//OutputDebugStringA(msgbuf);
		return 0;
	}
	case WM_CLOSE:	
	{
		if (MessageBox(hwnd, L"Really quit?", L"My application", MB_OKCANCEL) == IDOK)
		{
			DestroyWindow(hwnd);
		}		
	}
	
	case WM_SIZE:
	{
		int width = LOWORD(lParam);
		int height = HIWORD(lParam);

		OnSize(hwnd, (UINT)wParam, width, height);
	}
		return 0;

	}

	return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

